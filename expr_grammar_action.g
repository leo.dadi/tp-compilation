tokens SYM_EOF SYM_IDENTIFIER<string> SYM_INTEGER<int>
tokens SYM_LBRACE SYM_RBRACE SYM_LPARENTHESIS SYM_RPARENTHESIS SYM_SEMICOLON 
tokens SYM_ASSIGN SYM_IF SYM_ELSE SYM_WHILE SYM_RETURN SYM_COMMA
tokens SYM_ASTERISK SYM_DIV SYM_PLUS SYM_MINUS SYM_MOD
tokens SYM_EQUALITY SYM_NOTEQ SYM_LT SYM_LEQ SYM_GT SYM_GEQ SYM_MOD
tokens SYM_PRINT
non-terminals S INSTR INSTRS LINSTRS ELSE EXPR FACTOR
non-terminals LPARAMS REST_PARAMS
non-terminals IDENTIFIER INTEGER
non-terminals FUNDEF FUNDEFS
non-terminals ADD_EXPRS ADD_EXPR
non-terminals MUL_EXPRS MUL_EXPR
non-terminals CMP_EXPRS CMP_EXPR
non-terminals EQ_EXPRS EQ_EXPR
non-terminals BLOC
axiom S
{

  open Symbols
  open Ast
  open BatPrintf
  open BatBuffer
  open Batteries
  open Utils

  (* TODO *)
  let resolve_associativity term other =
       List.fold_left (fun acc tree -> Node (fst tree, [acc; snd tree]) ) term other


}

rules
S -> FUNDEFS SYM_EOF                                                            {Node (Tlistglobdef, $1)}
FUNDEFS     -> FUNDEF FUNDEFS                                                   {$1 @ $2}
FUNDEFS     ->                                                                  {[]}
FUNDEF      -> IDENTIFIER SYM_LPARENTHESIS LPARAMS SYM_RPARENTHESIS INSTR       {[Node(Tfundef,[$1;Node(Tfunargs,$3);$5])]}

LPARAMS     -> IDENTIFIER REST_PARAMS                                           {[Node(Targ,[$1])]@$2}
LPARAMS    ->                                                                   {[]}
REST_PARAMS -> SYM_COMMA IDENTIFIER REST_PARAMS                                 {[Node(Targ,[$2])]@$3}
REST_PARAMS ->                                                                  {[]}

INSTR       -> IDENTIFIER SYM_ASSIGN EXPR SYM_SEMICOLON                         {Node(Tassign,[Node(Tassignvar,[$1]@[$3])])}
INSTR       -> SYM_IF SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS BLOC ELSE          {Node(Tif,[$3]@[$5]@[$6])}
INSTR       -> SYM_WHILE SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS INSTR           {Node(Twhile,[$3]@[$5])}
INSTR       -> SYM_RETURN EXPR SYM_SEMICOLON                                    {Node(Treturn,[$2])}
INSTR       -> SYM_PRINT EXPR SYM_SEMICOLON                                     {Node(Tprint,[$2])}
INSTR       -> BLOC                                                             {$1}

EXPR        -> EQ_EXPR EQ_EXPRS                                                 {resolve_associativity $1 $2}

MUL_EXPR    -> FACTOR                                                           {$1}

FACTOR      -> INTEGER                                                          {$1}
FACTOR      -> IDENTIFIER                                                       {$1}
FACTOR      -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS                           {$2}

MUL_EXPRS   -> SYM_ASTERISK MUL_EXPR MUL_EXPRS                                  {[(Tmul,resolve_associativity $2 $3)]}
MUL_EXPRS   -> SYM_DIV MUL_EXPR MUL_EXPRS                                       {[(Tdiv,resolve_associativity $2 $3)]}
MUL_EXPRS   -> SYM_MOD MUL_EXPR MUL_EXPRS                                       {[(Tmod,resolve_associativity $2 $3)]}
MUL_EXPRS   ->                                                                  {[]}

ADD_EXPR    -> MUL_EXPR MUL_EXPRS                                               {resolve_associativity $1 $2}
ADD_EXPRS   -> SYM_PLUS ADD_EXPR ADD_EXPRS                                      {[(Tadd,resolve_associativity $2 $3)]}
ADD_EXPRS   -> SYM_MINUS ADD_EXPR ADD_EXPRS                                     {[(Tsub,resolve_associativity $2 $3)]}
ADD_EXPRS   ->                                                                  {[]}

EQ_EXPR     -> CMP_EXPR CMP_EXPRS                                               {resolve_associativity $1 $2}
EQ_EXPRS    -> SYM_EQUALITY EQ_EXPR EQ_EXPRS                                    {[(Tceq,resolve_associativity $2 $3)]}
EQ_EXPRS    -> SYM_NOTEQ EQ_EXPR EQ_EXPRS                                       {[(Tne,resolve_associativity $2 $3)]}
EQ_EXPRS    ->                                                                  {[]}

CMP_EXPR    -> ADD_EXPR ADD_EXPRS                                               {resolve_associativity $1 $2}
CMP_EXPRS   -> SYM_LT CMP_EXPR CMP_EXPRS                                        {[(Tclt,resolve_associativity $2 $3)]}
CMP_EXPRS   -> SYM_LEQ CMP_EXPR CMP_EXPRS                                       {[(Tcle,resolve_associativity $2 $3)]}
CMP_EXPRS   -> SYM_GT CMP_EXPR CMP_EXPRS                                        {[(Tcgt,resolve_associativity $2 $3)]}
CMP_EXPRS   -> SYM_GEQ CMP_EXPR CMP_EXPRS                                       {[(Tcge,resolve_associativity $2 $3)]}
CMP_EXPRS   ->                                                                  {[]}

INTEGER     -> SYM_INTEGER                                                      {IntLeaf($1)}
IDENTIFIER  -> SYM_IDENTIFIER                                                   {StringLeaf($1)}

ELSE        -> SYM_ELSE BLOC                                                    {$2}
ELSE        ->                                                                  {NullLeaf}
BLOC        -> SYM_LBRACE INSTRS SYM_RBRACE                                     {Node(Tblock, $2)}

INSTRS      -> INSTR INSTRS                                                     {($1)::$2}
INSTRS      ->                                                                  {[]}
