open Batteries
open Cfg

(* Analyse de vivacité *)

(* [vars_in_expr e] renvoie l'ensemble des variables qui apparaissent dans [e]. *)
let rec vars_in_expr (e: expr) =
   match e with
   Ebinop (binop,expr1,expr2) -> Set.union (vars_in_expr expr1) (vars_in_expr expr2)
   | Eunop (unop,expr)        -> vars_in_expr expr 
   | Eint int                 -> Set.empty
   | Evar string              -> Set.singleton string

(* [live_cfg_node node live_after] renvoie l'ensemble des variables vivantes
   avant un nœud [node], étant donné l'ensemble [live_after] des variables
   vivantes après ce nœud. *)
let live_cfg_node (node: cfg_node) (live_after: string Set.t) =
   match node with 
   Cassign (string,expr,int)  -> Set.union (vars_in_expr expr) ( Set.remove string live_after)
  | Creturn expr              -> Set.union (vars_in_expr expr) live_after
  | Cprint (expr,int)         -> Set.union (vars_in_expr expr) live_after
  | Ccmp (expr,s1,s2)         -> Set.union (vars_in_expr expr) live_after
  | Cnop int                  -> live_after

(* [live_after_node cfg n] renvoie l'ensemble des variables vivantes après le
   nœud [n] dans un CFG [cfg]. [lives] est l'état courant de l'analyse,
   c'est-à-dire une table dont les clés sont des identifiants de nœuds du CFG et
   les valeurs sont les ensembles de variables vivantes avant chaque nœud. *)
let live_after_node cfg n (lives: (int, string Set.t) Hashtbl.t) : string Set.t =
   let suiv = succs cfg n in
   Set.fold ( fun node acc -> Set.union ( match Hashtbl.find_option lives node with
                                                         |Some stringset -> stringset
                                                         |None -> Set.empty) acc ) suiv Set.empty


(* [live_cfg_nodes cfg lives] effectue une itération du calcul de point fixe.

   Cette fonction met à jour l'état de l'analyse [lives] et renvoie un booléen
   qui indique si le calcul a progressé durant cette itération (i.e. s'il existe
   au moins un nœud n pour lequel l'ensemble des variables vivantes avant ce
   nœud a changé). *)
let live_cfg_nodes cfg (lives : (int, string Set.t) Hashtbl.t) =
   let list = List.sort compare (Hashtbl.to_list cfg) in
   List.fold_left ( fun acc (index,node) -> 
      let live_out = live_after_node cfg index lives in
      let live_in = live_cfg_node node live_out in
      match Hashtbl.find_option lives index with
      | Some live_in_temp -> 
         if (Set.equal live_in_temp live_in) then acc else (
         (Hashtbl.replace lives index live_in); true)
      | None -> (Hashtbl.add lives index live_in); true) false list




(* [live_cfg_fun f] calcule l'ensemble des variables vivantes avant chaque nœud
   du CFG en itérant [live_cfg_nodes] jusqu'à ce qu'un point fixe soit atteint.
   *)
let live_cfg_fun (f: cfg_fun) : (int, string Set.t) Hashtbl.t =
   let lives = Hashtbl.create 17 in
   while (live_cfg_nodes f.cfgfunbody lives) 
   do ()
   done;
   lives
