open Ast
open Elang
open Prog
open Report
open Options
open Batteries
open Elang_print
open Utils

let tag_is_binop =
  function
    Tadd -> true
  | Tsub -> true
  | Tmul -> true
  | Tdiv -> true
  | Tmod -> true
  | Txor -> true
  | Tcle -> true
  | Tclt -> true
  | Tcge -> true
  | Tcgt -> true
  | Tceq -> true
  | Tne  -> true
  | _    -> false

let binop_of_tag =
  function
    Tadd -> Eadd
  | Tsub -> Esub
  | Tmul -> Emul
  | Tdiv -> Ediv
  | Tmod -> Emod
  | Txor -> Exor
  | Tcle -> Ecle
  | Tclt -> Eclt
  | Tcge -> Ecge
  | Tcgt -> Ecgt
  | Tceq -> Eceq
  | Tne -> Ecne
  | _ -> assert false

(* [make_eexpr_of_ast a] builds an expression corresponding to a tree [a]. If
   the tree is not well-formed, fails with an [Error] message. *)
let rec make_eexpr_of_ast (a: tree) : expr res =
  let res =
    match a with
    | Node(t, [e1; e2]) when tag_is_binop t ->
              OK( Ebinop (binop_of_tag t, (make_eexpr_of_ast e1 >>! fun x -> x), (make_eexpr_of_ast e2 >>! fun x -> x)))
    | StringLeaf a -> 
              OK ( Evar a)
    | IntLeaf n -> 
              OK ( Eint n)
    | Node(Tneg, [IntLeaf a]) -> 
              OK ( Eunop (Eneg, Eint a))
    | _ -> Error (Printf.sprintf "Unacceptable ast in make_eexpr_of_ast %s"
                    (string_of_ast a))
  in
  match res with
    OK o -> res
  | Error msg -> Error (Format.sprintf "In make_eexpr_of_ast %s:\n%s"
                          (string_of_ast a) msg)

let rec make_einstr_of_ast (a: tree) : instr res =
  let res =
    match a with
    | Node(Tassign, [Node(Tassignvar, [StringLeaf a; expr])]) ->
            OK ( Iassign ( a, make_eexpr_of_ast expr >>! fun x -> x))
    | Node(Tif, [cond;instr;NullLeaf]) ->
            make_eexpr_of_ast cond >>= fun x ->
            make_einstr_of_ast instr >>= fun y->
              OK( Iif(x,y,Iblock([])))
    | Node(Tif, [cond;instr;instr2]) ->
            make_eexpr_of_ast cond >>= fun x ->
            make_einstr_of_ast instr >>= fun y->
            make_einstr_of_ast instr2 >>= fun z->
              OK( Iif(x,y,z))
    | Node(Twhile, [expr;instr]) ->
            OK ( Iwhile ( make_eexpr_of_ast expr >>! (fun x->x), make_einstr_of_ast instr >>! (fun x->x)))
    | Node(Tblock, l) ->
            OK ( Iblock ( 
              List.fold_right( fun x acc -> (make_einstr_of_ast x >>! (fun x->x))::acc) l []
            ))      
    | Node(Treturn, [expr]) -> 
            OK ( Ireturn ( make_eexpr_of_ast expr >>! (fun x-> x) ))
    | Node(Tprint, [expr]) -> 
      OK ( Iprint ( make_eexpr_of_ast expr >>! (fun x-> x) ))

    | _ -> Error (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s"
                    (string_of_ast a))
  in
  match res with
    OK o -> res
  | Error msg -> Error (Format.sprintf "In make_einstr_of_ast %s:\n%s"
                          (string_of_ast a) msg)

let make_ident (a: tree) : string res =
  match a with
  | Node (Targ, [s]) ->
    OK (string_of_stringleaf s)
  | a -> Error (Printf.sprintf "make_ident: unexpected AST: %s"
                  (string_of_ast a))

let make_fundef_of_ast (a: tree) : (string * efun) res =
  match a with
  | Node (Tfundef, [StringLeaf fname; Node (Tfunargs, fargs);l]) ->
    list_map_res make_ident fargs >>= fun fargs ->
     (* TODO *)
     make_einstr_of_ast l >>= fun x->
      OK ( (fname,
      {  funargs = fargs;
        funbody = x;
     }))
  | _ ->
    Error (Printf.sprintf "make_fundef_of_ast: Expected a Tfundef, got %s."
             (string_of_ast a))

let make_eprog_of_ast (a: tree) : eprog res =
  match a with
  | Node (Tlistglobdef, l) ->
    list_map_res (fun a -> make_fundef_of_ast a >>= fun (fname, efun) -> OK (fname, Gfun efun)) l
  | _ ->
    Error (Printf.sprintf "make_fundef_of_ast: Expected a Tlistglobdef, got %s."
             (string_of_ast a))

let pass_elang ast =
  match make_eprog_of_ast ast with
  | Error msg ->
    record_compile_result ~error:(Some msg) "Elang";
    Error msg
  | OK  ep ->
    dump !e_dump dump_e ep (fun file () ->
        add_to_report "e" "E" (Code (file_contents file))); OK ep

